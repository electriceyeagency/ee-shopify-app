import React from 'react';
import gql from 'graphql-tag';
import { Mutation, Query } from 'react-apollo';
import {
  Banner,
  Button,
  Card,
  EmptyState,
  DisplayText,
  Frame,
  Layout,
  ResourceList,
  Stack,
  TextStyle,
  Thumbnail,
  Toast
} from '@shopify/polaris';
import store from 'store-js';
import { Redirect } from '@shopify/app-bridge/actions';
import { Context } from '@shopify/app-bridge-react';
import { DeleteMinor, RefreshMinor } from '@shopify/polaris-icons';

const img = 'https://cdn.shopify.com/s/files/1/0757/9955/files/empty-state.svg';

const LIST_SCRIPT_TAGS = gql`
  {
    scriptTags(first:10) {
      edges {
        node {
          id
          src
          updatedAt
          createdAt
        }
      }
    }
  }
`;

const CREATE_SCRIPT_TAG = gql`
  mutation scriptTagCreate($input: ScriptTagInput!) {
    scriptTagCreate(input: $input) {
      scriptTag {
        id
        src
        updatedAt
        createdAt
      }
      userErrors {
        field
        message
      }
    }
  }
`;

const UPDATE_SCRIPT_TAG = gql`
  mutation scriptTagUpdate($id: ID!, $input: ScriptTagInput!) {
    scriptTagUpdate(id: $id, input: $input) {
      scriptTag {
        id
        src
        updatedAt
        createdAt
      }
      userErrors {
        field
        message
      }
    }
  }
`;

const DELETE_SCRIPT_TAG = gql`
  mutation scriptTagDelete($id: ID!) {
    scriptTagDelete(id: $id) {
      deletedScriptTagId
      userErrors {
        field
        message
      }
    }
  }
`;

class ScriptCRUD extends React.Component {
  static contextType = Context;

  state = {
    showCreateToast: false,
    showUdpateToast: false,
    showDeleteToast: false,
  };

  render() {
    // const app = this.context;
    // const redirectToProduct = () => {
    //   const redirect = Redirect.create(app);
    //   redirect.dispatch(
    //     Redirect.Action.APP,
    //     '/edit-discount-product',
    //   );
    // };

    return (
      <Query query={LIST_SCRIPT_TAGS}>
        {({ data, loading, error }) => {
          if (loading) return <div>Loading…</div>;
          if (error) return <div>{error.message}</div>;
          let customScripts = store.get('customScripts');

          return (
            <Frame>
              {this.isStateEmpty() ? (
                <div>
                  <Mutation
                    mutation={CREATE_SCRIPT_TAG}
                  >
                    {(handleSubmit, {err: createErr, data: createData}) => {
                      const showError = createErr && (
                        <Banner status="critical">{createErr.message}</Banner>
                      );
                      const showToast = createData && this.state.showCreateToast && (
                        <Toast
                          content="Sucessfully added"
                          onDismiss={() => this.setState({ showCreateToast: false })}
                        />
                      );

                      return (
                        <Layout>
                          {showToast}
                          <Layout.Section>
                            {showError}
                          </Layout.Section>

                          <EmptyState
                            heading="Add EE Script"
                            action={{
                              content: 'Add Script',
                              onAction: () => {
                                handleSubmit({
                                  variables: { 
                                    input: {
                                      displayScope: 'ALL',
                                      src: this.context.localOrigin + '/frontend.js?' + new Date().getTime()
                                    }
                                  }
                                }).then((res) => {
                                  if(res.data) {
                                    store.set('customScripts', [res.data.scriptTagCreate.scriptTag]);
                                    this.setState({ showCreateToast: true });
                                  }
                                })
                              }
                            }}
                            image={img}
                          >
                          <p>Add EE's custom scripts to your site</p>
                          </EmptyState>
                        </Layout>
                      )
                    }}
                  </Mutation>
                </div>
              ) : (
                <Layout>
                  <Layout.Section>
                    <DisplayText size="large">Edit Scripts</DisplayText>
                  </Layout.Section>
                  <Layout.Section>
                    <Card>
                      <ResourceList
                        showHeader
                        resourceName={{ singular: 'Script', plural: 'Scripts' }}
                        items={customScripts}
                        renderItem={item => {
                          return (
                            <ResourceList.Item
                              id={item.id}
                              accessibilityLabel={`${item.src}`}
                            >
                              <Stack>
                                <Stack.Item fill>
                                  <h3>
                                    <TextStyle variation="strong">
                                      {item.src}
                                    </TextStyle>
                                  </h3>
                                </Stack.Item>
                                  <TextStyle>
                                    Updated: {new Date(item.updatedAt).toLocaleString()}
                                  </TextStyle>
                                <Stack.Item>
                                </Stack.Item>
                                <Stack.Item>
                                  <Mutation mutation={UPDATE_SCRIPT_TAG}>
                                    {(handleUpdate, {err: updateErr, data: updateData}) => {
                                      const showError = updateErr && (
                                        <Banner status="critical">{updateErr.message}</Banner>
                                      );
                                      const showToast = updateData && this.state.showUpdateToast && (
                                        <Toast
                                          content="Sucessfully updated"
                                          onDismiss={() => this.setState({ showUpdateToast: false })}
                                        />
                                      );

                                      return (
                                        <div>
                                          {showError}
                                          {showToast}
                                          <Button icon={RefreshMinor} onClick={() => { 
                                            handleUpdate({
                                              variables: { id: item.id, input: {
                                                src: this.context.localOrigin + '/frontend.js?' + new Date().getTime()
                                              }},
                                            }).then((res) => {
                                              if(res.data) {
                                                const updatedId = res.data.scriptTagUpdate.scriptTag.id;
                                                let filteredCustomScripts = store.get('customScripts');
                                                filteredCustomScripts.forEach((script, index) => {
                                                  if(script.id === updatedId) {
                                                    filteredCustomScripts[index] = res.data.scriptTagUpdate.scriptTag;
                                                  }
                                                });

                                                store.set('customScripts', filteredCustomScripts);
                                                this.setState({ showUpdateToast: true });
                                              }
                                            })

                                          }}></Button>
                                        </div>
                                      )
                                    }}
                                  </Mutation>
                                </Stack.Item>
                                <Stack.Item>
                                  <Mutation mutation={DELETE_SCRIPT_TAG}>
                                    {(handleDelete, {err: deleteErr, data: deleteData}) => {
                                      const showError = deleteErr && (
                                        <Banner status="critical">{deleteErr.message}</Banner>
                                      );
                                      const showToast = deleteData && this.state.showDeleteToast && (
                                        <Toast
                                          content="Sucessfully deleted"
                                          onDismiss={() => this.setState({ showDeleteToast: false })}
                                        />
                                      );

                                      return (
                                        <div>
                                          {showError}
                                          {showToast}
                                          <Button icon={DeleteMinor} onClick={() => {
                                            handleDelete({
                                              variables: { id: item.id },
                                            }).then((res) => {
                                              if(res.data) {
                                                const deletedId = res.data.scriptTagDelete.deletedScriptTagId;
                                                let filteredCustomScripts = store.get('customScripts');
                                                filteredCustomScripts.filter((script, index) => {
                                                  if(script.id === deletedId) {
                                                    filteredCustomScripts.splice(index, 1);
                                                  }
                                                });

                                                store.set('customScripts', filteredCustomScripts);
                                                this.setState({ showDeleteToast: true });
                                              }
                                            })
                                          }}></Button>
                                        </div>
                                      )
                                    }}
                                  </Mutation>
                                </Stack.Item>
                              </Stack>
                            </ResourceList.Item>
                          );
                        }}
                      />
                    </Card>
                  </Layout.Section>
                </Layout>
              )}
            </Frame>
          );
        }}
      </Query>
    )
  }

  isStateEmpty = () => {
    const customScripts = store.get('customScripts');
    if(!customScripts || customScripts.length <= 0) return true
  };
}

export default ScriptCRUD;

              