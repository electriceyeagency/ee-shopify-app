import React from 'react';
import gql from 'graphql-tag';
import { Mutation, Query } from 'react-apollo';
import {
  Banner,
  Button,
  Card,
  ResourceList,
  Stack,
  TextStyle,
  Thumbnail,
  Toast
} from '@shopify/polaris';
import store from 'store-js';
import { Redirect } from '@shopify/app-bridge/actions';
import { Context } from '@shopify/app-bridge-react';
import { DeleteMinor } from '@shopify/polaris-icons';

const GET_PRODUCTS_BY_ID = gql`
  query getProducts($ids: [ID!]!) {
    nodes(ids: $ids) {
      ... on Product {
        title
        handle
        descriptionHtml
        id
        images(first: 1) {
          edges {
            node {
              originalSrc
              altText
            }
          }
        }
        variants(first: 1) {
          edges {
            node {
              price
              id
            }
          }
        }
      }
    }
  }
`;

const DELETE_METAFIELD = gql`
  mutation metafieldDelete($input: MetafieldDeleteInput!) {
    metafieldDelete(input: $input) {
      deletedId
      userErrors {
        field
        message
      }
    }
  }
`;

class BundleProductsList extends React.Component {
  static contextType = Context;

  render() {
    const { rerender } = this.props;
    
    return (
      <Query query={GET_PRODUCTS_BY_ID} variables={{ ids: store.get('bundleIds') }}>
        {({ data, loading, error }) => {
          if (loading) return <div>Loading…</div>;
          if (error) return <div>{error.message}</div>;

          return (
            <Card>
              <ResourceList
                showHeader
                resourceName={{ singular: 'Product', plural: 'Products' }}
                items={data.nodes}
                renderItem={(item, index) => {
                  const media = (
                    <Thumbnail
                      source={
                        item.images.edges[0]
                          ? item.images.edges[0].node.originalSrc
                          : ''
                      }
                      alt={
                        item.images.edges[0]
                          ? item.images.edges[0].node.altText
                          : ''
                      }
                    />
                  );
                  const price = item.variants.edges[0].node.price;
                  return (
                    <ResourceList.Item
                      id={item.id}
                      media={media}
                      accessibilityLabel={`${item.title}`}
                      onClick={() => {
                        // store.set('editCustomProduct', item);
                        // redirectToProduct();
                      }}
                    >
                      <Stack>
                        <Stack.Item fill>
                          <h3>
                            <TextStyle variation="strong">
                              {item.title}
                            </TextStyle>
                          </h3>
                        </Stack.Item>
                        <Stack.Item>
                          <Mutation mutation={DELETE_METAFIELD}>
                            {(handleDelete, {err: deleteErr, data: deleteData}) => {
                              const showError = deleteErr && (
                                <Banner status="critical">{deleteErr.message}</Banner>
                              );

                              return (
                                <div>
                                  {showError}
                                  <Button icon={DeleteMinor} onClick={() => {
                                    const deleteMetafield = store.get('metafields').find((field => field.key == item.handle ))
                                    if(deleteMetafield) {
                                      handleDelete({ variables: { input: { id: deleteMetafield.id }} }).then((res) => {
                                        if(res.data) {
                                          this.removeProduct(item);

                                          if(store.get('bundleIds').length <= 0) {
                                            rerender();
                                          }
                                        }
                                      });
                                    } else {
                                      // shouldn't be used anymore, but a fallback just incase
                                      this.removeProduct(item);
                                      this.forceUpdate();

                                      if(store.get('bundleIds').length <= 0) {
                                        rerender();
                                      }
                                    }
                                  }}></Button>
                                </div>
                              )
                            }}
                          </Mutation>
                        </Stack.Item>
                      </Stack>
                    </ResourceList.Item>
                  );
                }}
              />
            </Card>
          );
        }}
      </Query>
    );
  }

  removeProduct = (sentProduct) => {
    const filteredIds = store.get('bundleIds').filter((id) => id !== sentProduct.id);
    const filteredData = store.get('bundleData').filter((product) => sentProduct.id !== product.id);

    store.set('bundleIds', filteredIds);
    store.set('bundleData', filteredData);

    this.props.deleteToast();
  }
}

export default BundleProductsList;
