# Shopify Node App

.env file requirements:

```
SHOPIFY_API_KEY=''
SHOPIFY_API_SECRET=''
SHOPIFY_APP_URL=''
SHOPIFY_API_SCOPES=read_products,write_products
```

## Prerequisites

We need to have an app set up in the shopify partners app section, with proper URLs pointed to the ngrok tunnel, with a proper redirect, and above parameters, with app url corresponding to the url provided above.

## Install

`yarn`

## Dev:

Install [ngrok](https://ngrok.com/)

Run ngrok

`ngrok http 3000`

Run Server

`yarn dev`

## Production

`git push heroku master`
