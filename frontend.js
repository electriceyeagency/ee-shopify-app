/*---------
  EE CUSTOM APP FRONTEND
  VERSION: 0.0.1
  THEME: Debut
  AUTHOR: Mike Weaver
  USAGE:

  This code needs to be in the theme ("product-template.liquid" for Debut)
  <!-- ee -->
  {% if product.metafields.upsell.size > 0 %}
  <div id="eeUpsell"></div>
    <script>
      window.eeUpsell = [
        {%- for upsell in product.metafields.upsell -%}
        {%- if forloop.index != 1 -%},{%- endif -%}
          {
            id: '{{ upsell | first }}',
            value: {{ upsell | last }}
          }
        {%- endfor -%}
      ];
    </script>
  {% endif %}
  <br>
  {% if product.metafields.bundle.size > 0 %}
  <div id="eeBundle"></div>
    <script>
      window.eeBundle = [
        {
          id: '{{ product.handle }}',
          value: {{ product | json }}
        }{% if product.metafields.bundle.size > 0 %},{% endif %}
        {%- for bundle in product.metafields.bundle -%}
        {%- if forloop.index != 1 -%},{%- endif -%}
          {
            id: '{{ bundle | first }}',
            value: {{ bundle | last }}
          }
        {%- endfor -%}
      ];
    </script>
  {% endif %}
  <br>
  <!-- end ee -->
---------*/

var eeAppScript = {
  //---------
  // INITIALIZE 
  //---------
  init: function() {
    console.log('EE APP SCRIPT LOADED', new Date().toLocaleString());
    this.upsellMount = document.getElementById('eeUpsell') || false;
    this.upsellData = window.eeUpsell || false;

    if(window.eeUpsell && this.upsellMount) {
      this.upsellMount.innerHTML = '<h3>Frequently Bought Together</h3>';
      this.upsellMount.innerHTML = this.upsellMount.innerHTML + this.generateUpsellHTML();
      this.addEventListeners();
    }

    this.bundleMount = document.getElementById('eeBundle') || false;
    this.bundleData = window.eeBundle || false;

    if(window.eeBundle && this.bundleMount) {
      this.bundleMount.innerHTML = '<h3>Bundle Together</h3>';
      this.bundleMount.innerHTML = this.bundleMount.innerHTML + this.generateBundleHTML() + this.generateAddAllButton();
      this.addEventListeners();
    }
  },

  //---------
  // MARKUP 
  //---------
  generateUpsellHTML: function() {
    var self = this;
    var productsHTML = '';

    this.upsellData.forEach(function(product) {
      productsHTML += self.generateUpsellProduct(product.value);
    })

    return '<div style="' + 
      'display: grid;' +
      'grid-auto-columns: 1fr;' +
      'grid-template-areas: \'a a\'; ' +
      'gap: 1.25em;' +
    '">' + 
      productsHTML + 
    '</div>';
  },

  generateBundleHTML: function() {
    var self = this;
    var productsHTML = '';
    
    this.bundleData.forEach(function(product) {
      productsHTML += self.generateBundleProduct(product.value);
    })

    return '<div style="' + 
      'display: flex;' +
      'justify-content: space-between;' +
    '">' + 
      productsHTML + 
    '</div>';
  },

  generateUpsellProduct: function(sentProduct) {
    return (
      '<div>' + 
        '<a href="/products/' + sentProduct.handle + '">' + 
          '<p><img class="lazyload upsell-image" style="width: 100%;" ' + 
            'src="' + this.getSizedImageUrl(sentProduct.images[0].originalSrc, '100x') + '" ' +
            'data-src="' + this.getSizedImageUrl(sentProduct.images[0].originalSrc, '1x1').replace('1x1', '{width}x') + '" ' +
            'data-widths="[180, 360, 540, 720, 900, 1080, 1296, 1512, 1728, 2048]" ' +
            'data-sizes="auto" ' +
          '/></p>' + 
          '<h5 style="margin-bottom: 0.25em;">' + sentProduct.title + '</h5>' + 
          '<p class="upsell-price">$' + sentProduct.variants[0].price + '</p>' + 
        '</a>' +
        '<br>' +  
        this.generateSelect(sentProduct) +
        '<button ' + 
          'class="btn"' +
          'data-variant-id="' + sentProduct.variants[0].id.match(/\d+/g) + '"' +
          'ee-atc data-add-to-cart ' + 
          'style="width: 100%"' +
        '>' +
          '<span data-add-to-cart-text>Add To Cart</span>' +
          '<span class="hide" data-loader>' +
            '<svg aria-hidden="true" focusable="false" role="presentation" class="icon icon-spinner" viewBox="0 0 20 20"><path d="M7.229 1.173a9.25 9.25 0 1 0 11.655 11.412 1.25 1.25 0 1 0-2.4-.698 6.75 6.75 0 1 1-8.506-8.329 1.25 1.25 0 1 0-.75-2.385z" fill="#919EAB"/></svg>' + 
          '</span>' +
        '</button>' +
        '<div class="product-form__error-message-wrapper product-form__error-message-wrapper--hidden" data-error-message-wrapper role="alert">' +
          '<svg aria-hidden="true" focusable="false" role="presentation" class="icon icon-error" viewBox="0 0 14 14"><g fill="none" fill-rule="evenodd"><path d="M7 0a7 7 0 0 1 7 7 7 7 0 1 1-7-7z"/><path class="icon-error__symbol" d="M6.328 8.396l-.252-5.4h1.836l-.24 5.4H6.328zM6.04 10.16c0-.528.432-.972.96-.972s.972.444.972.972c0 .516-.444.96-.972.96a.97.97 0 0 1-.96-.96z"/></g></svg>' +
          '<span class="product-form__error-message" data-error-message></span>' +
        '</div>' +
      '</div>'
    )
  },

  generateBundleProduct: function(sentProduct) {
    var image = sentProduct.images[0];
    if(image.originalSrc) {
      image = image.originalSrc;
    }
    
    return (
      '<div>' + 
        '<a href="/products/' + sentProduct.handle + '">' + 
          '<p><img class="lazyload upsell-image" style="width: 100%;" ' + 
            'src="' + this.getSizedImageUrl(image, '100x') + '" ' +
            'data-src="' + this.getSizedImageUrl(image, '1x1').replace('1x1', '{width}x') + '" ' +
            'data-widths="[180, 360, 540, 720, 900, 1080, 1296, 1512, 1728, 2048]" ' +
            'data-sizes="auto" ' +
          '/></p>' + 
          '<h5 style="margin-bottom: 0.25em;">' + sentProduct.title + '</h5>' + 
          '<p class="upsell-price">$' + sentProduct.variants[0].price + '</p>' + 
        '</a>' +
      '</div>'
    )
  },

  generateSelect: function(sentProduct) {
    var self = this;
    var options = '';
    sentProduct.variants.forEach(function(variant, index) {
      options += (
        '<option ' + 
          'value="' + variant.id.match(/\d+/g) + '" ' +
          'data-price="$' + variant.price + '" ' +
          'data-image="' + (
            variant.image && variant.image.originalSrc || sentProduct.images[0].originalSrc
          ) + '" ' +
          'data-available="' + variant.availableForSale + '" ' +
          (!variant.availableForSale ? 'disabled="disabled" ' : '') + 
          (index === 0 ? 'selected="selected"' : '') +
        '>' + 
          variant.title +
        '</option>'
      )
    });

    return '<select ' + 
      'style="width: 100%; margin-bottom: 0.75em; ' + 
      (sentProduct.variants.length <= 1 ? 'display: none;' : '') + '" ' + 
      'class="section-variant">' + 
      options + 
    '</select>'
  },

  generateAddAllButton: function() {
    return '<br>' + 
    '<button ' + 
      'class="btn" ' + 
      'style="width: 100%" ' + 
      'id="addBundle" ' +
      'data-add-to-cart' + 
    '>' +
      '<span data-add-to-cart-text>Add All To Cart</span>' +
        '<span class="hide" data-loader>' +
          '<svg aria-hidden="true" focusable="false" role="presentation" class="icon icon-spinner" viewBox="0 0 20 20"><path d="M7.229 1.173a9.25 9.25 0 1 0 11.655 11.412 1.25 1.25 0 1 0-2.4-.698 6.75 6.75 0 1 1-8.506-8.329 1.25 1.25 0 1 0-.75-2.385z" fill="#919EAB"/></svg>' + 
        '</span>' +
      '</span>' +
    '</button>' + 
    '<div class="product-form__error-message-wrapper product-form__error-message-wrapper--hidden" data-error-message-wrapper role="alert">' +
      '<svg aria-hidden="true" focusable="false" role="presentation" class="icon icon-error" viewBox="0 0 14 14"><g fill="none" fill-rule="evenodd"><path d="M7 0a7 7 0 0 1 7 7 7 7 0 1 1-7-7z"/><path class="icon-error__symbol" d="M6.328 8.396l-.252-5.4h1.836l-.24 5.4H6.328zM6.04 10.16c0-.528.432-.972.96-.972s.972.444.972.972c0 .516-.444.96-.972.96a.97.97 0 0 1-.96-.96z"/></g></svg>' +
      '<span class="product-form__error-message" data-error-message></span>' +
    '</div>';
  },

  //---------
  // EVENT lISTENERS 
  //---------
  addEventListeners: function() {
    var self = this;

    //add click event listener to all special buttons
    var buttons = document.querySelectorAll('button[ee-atc]')
    buttons.forEach(function(el) {
      el.addEventListener('click', self.addToCart.bind(self, el));
    });

    var addAllButton = document.getElementById('addBundle')
    if(addAllButton) {
      addAllButton.addEventListener('click', self.addAllToCart.bind(self, addAllButton));
    }

    var selects = document.querySelectorAll('.section-variant')
    selects.forEach(function(el) {
      //trigger one time
      self.changeVariant.call(self, el);

      //check for oos selected
      self.checkOOS.call(self, el);

      //add event listener
      el.addEventListener('change', self.changeVariant.bind(self, el));
    });
  },

  changeVariant: function(sentEl) {
    var self = this;
    var sentElId = sentEl.value;
    var sentElAvailable = sentEl.querySelector('option:checked').dataset.available === 'true';
    var sentPrice = sentEl.querySelector('option:checked').dataset.price;
    var sentImage = sentEl.querySelector('option:checked').dataset.image;
    var parentDiv = sentEl.closest('div');
    var price = parentDiv.querySelector('.upsell-price');
    var buttonEl = parentDiv.querySelector('button[data-add-to-cart]');
    var imageEl = parentDiv.querySelector('.upsell-image');
    var linkEl = parentDiv.querySelector('a');

    if(buttonEl && buttonEl.dataset && buttonEl.dataset.variantId && sentElId) {
      buttonEl.dataset.variantId = sentElId;
      linkEl.href = linkEl.href.split('?')[0] + '?variant=' + sentElId;

      if(sentElAvailable) {
        buttonEl.querySelector('span').innerHTML = 'Add to cart';
        buttonEl.removeAttribute('aria-disabled');
      } else {
        buttonEl.querySelector('span').innerHTML = 'Sold out';
        buttonEl.setAttribute('aria-disabled', true);
      }
    }

    if(imageEl && !sentImage.includes('no-image')) {
      imageEl.classList.remove('lazyloaded');
      imageEl.dataset.srcset = '';
      imageEl.srcset = '';
      imageEl.src = this.getSizedImageUrl(sentImage, '100x');

      imageEl.dataset.src = this.getSizedImageUrl(sentImage, '1x1').replace('1x1', '{width}x');;
      imageEl.classList.add('lazyload');
    }

    price.innerHTML = sentPrice;
  },

  checkOOS: function(sentEl) {
    var sentElAvailable = sentEl.querySelector('option:checked').dataset.available === 'true';
    if(!sentElAvailable) {
      sentEl.querySelector('option[data-available="true"]').selected = 'selected';
      this.changeVariant.call(this, sentEl);
    }
  },

  //---------
  // AJAX FUNCTIONALITY 
  //---------
  post: function(data, callback) {
    var request = new XMLHttpRequest();
    request.onreadystatechange = function() {
      if (request.readyState === 4) {
        callback(request);
      }
    }

    request.open("POST", data.url, true);
    request.setRequestHeader('Content-Type', 'application/json');
    // https://community.shopify.com/c/Shopify-Design/AJAX-POST-cart-add-js-NEVER-returns-422-only-200-OK-on/td-p/375736
    request.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    request.send(JSON.stringify(data.data));
  },

  addToCart: function(sentEl, bulkData) {
    var self = this;
    
    if(!bulkData) {
      var id = sentEl.dataset.variantId;
      var parentDiv = sentEl.closest('div');
      var themeProduct = new window.theme.Product(parentDiv);
    } else {
      var parentDiv = sentEl.closest('#eeBundle');
    }


    this.toggleButtonLoading(sentEl, true);

    this.post(
      {
        url: '/cart/add.js',
        data: { items: bulkData } || {
          id: id,
          form_type: 'product'
        }
      },
      function(data) {
        if (data.status === 200) {
          if(!bulkData) {
            themeProduct._setupCartPopup(JSON.parse(data.responseText));
          } else {
            window.location.href = '/cart';
          }

          self.toggleButtonLoading(sentEl, false);
        } else {
          var parentEl = sentEl.closest('div');
          self.toggleButtonLoading(sentEl, false);
          parentEl.querySelector('div[data-error-message-wrapper]').style.display = 'block';
          parentEl.querySelector('span[data-error-message]').innerHTML = JSON.parse(data.responseText).description;
        }
      }
    );
  },

  addAllToCart: function(sentEl) {
    var bundleData = []
    this.bundleData.forEach(function(product) {
      bundleData.push({
        id: product.value.variants[0].id.toString().match(/(\d+)/)[0],
        quantity: 1
      });
    })

    this.addToCart(sentEl, bundleData)
  },

  toggleButtonLoading: function(sentEl, disable) {
    if(disable) {
      sentEl.querySelector('span[data-add-to-cart-text]').classList.add('hide');
      sentEl.querySelector('span[data-loader]').classList.remove('hide');
    } else {
      sentEl.querySelector('span[data-add-to-cart-text]').classList.remove('hide');
      sentEl.querySelector('span[data-loader]').classList.add('hide');
    }
  },

  //---------
  // HELPERS
  //---------
  getSizedImageUrl: function(src, size) {
     function removeProtocol(path) {
      return path.replace(/http(s)?:/, '')
    }
    if (size === null) {
      return src
    }

    if (size === 'master') {
      return removeProtocol(src)
    }

    var match = src.match(/\.(jpg|jpeg|gif|png|bmp|bitmap|tiff|tif)(\?v=\d+)?$/i)

    if (match) {
      var prefix = src.split(match[0])
      var suffix = match[0]

      return removeProtocol(prefix[0] + '_' + size + suffix)
    }

    return null
  }
};

eeAppScript.init();

