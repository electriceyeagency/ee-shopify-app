const fs = require('fs');
require('isomorphic-fetch');
const dotenv = require('dotenv');
const Koa = require('koa');
const send = require('koa-send');
const next = require('next');
const { default: createShopifyAuth } = require('@shopify/koa-shopify-auth');
const { verifyRequest } = require('@shopify/koa-shopify-auth');
const { default: Shopify, ApiVersion } = require('@shopify/shopify-api');
const Router = require('koa-router');

dotenv.config();

Shopify.Context.initialize({
  API_KEY: process.env.SHOPIFY_API_KEY,
  API_SECRET_KEY: process.env.SHOPIFY_API_SECRET,
  SCOPES: process.env.SHOPIFY_API_SCOPES.split(","),
  HOST_NAME: process.env.SHOPIFY_APP_URL.replace(/https:\/\//, ""),
  API_VERSION: ApiVersion.January21,
  IS_EMBEDDED_APP: true,
  SESSION_STORAGE: new Shopify.Session.MemorySessionStorage(),
});

const port = parseInt(process.env.PORT, 10) || 3000;
const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();

// @TODO connect this to mongodb
const ACTIVE_SHOPIFY_SHOPS = {};

app.prepare().then(() => {
  const server = new Koa();
  const router = new Router();
  server.keys = [Shopify.Context.API_SECRET_KEY];

  server.use(
    createShopifyAuth({
      async afterAuth(ctx) {
        const { shop, scope, accessToken } = ctx.state.shopify;
        ACTIVE_SHOPIFY_SHOPS[shop] = scope;
        // @TODO set "tier" level
        // ACTIVE_SHOPIFY_SHOPS[shop].tier = 'pro';

        // https://github.com/Shopify/polaris-react/issues/979
        // https://community.shopify.com/c/Shopify-APIs-SDKs/shopOrigin-must-be-provided-Error-Step-3-Build-shopify-add-with/td-p/741993
        ctx.cookies.set("shopOrigin", shop, {
          httpOnly: false,
          secure: true,
          sign: true,
          sameSite: "none"
        });

        const registration = await Shopify.Webhooks.Registry.register({
          shop,
          accessToken,
          path: '/webhooks',
          topic: 'APP_UNINSTALLED',
          apiVersion: ApiVersion.January21,
          webhookHandler: (_topic, shop, _body) => {
            console.log('App uninstalled');
            delete ACTIVE_SHOPIFY_SHOPS[shop];
          },
        });

        if (registration.success) {
          console.log('Successfully registered webhook!');
          // @TODO integrate with a webhook
        } else {
          console.log('Failed to register webhook', registration.result);
        }

        const returnUrl = `https://${Shopify.Context.HOST_NAME}?shop=${shop}`;
        ctx.redirect(returnUrl);
      },
    }),
  );

  router.post("/graphql", verifyRequest({returnHeader: true}), async (ctx, next) => {
    await Shopify.Utils.graphqlProxy(ctx.req, ctx.res);
  });

  router.post('/webhooks', async (ctx) => {
    await Shopify.Webhooks.Registry.process(ctx.req, ctx.res);
    console.log(`Webhook processed with status code 200`);
  });

  const handleRequest = async (ctx) => {
    await handle(ctx.req, ctx.res);
    ctx.respond = false;
    ctx.res.statusCode = 200;
  };

  router.get("/", async (ctx) => {
    const shop = ctx.query.shop;

    if (ACTIVE_SHOPIFY_SHOPS[shop] === undefined) {
      ctx.redirect(`/auth?shop=${shop}`);
    } else {
      await handleRequest(ctx);
    }
  });

  router.get("/frontend.js", async (ctx) => {
    await send(ctx, ctx.path, { root: __dirname });
  });

  router.get("(/_next/static/.*)", handleRequest);
  router.get("/_next/webpack-hmr", handleRequest);
  router.get("(.*)", verifyRequest(), handleRequest);

  server.use(router.allowedMethods());
  server.use(router.routes());

  server.listen(port, () => {
    console.log(`🚀 Ready on http://localhost:${port} 🚀`);
  });
});
