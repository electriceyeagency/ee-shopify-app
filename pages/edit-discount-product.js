import React from 'react';
import {
  Banner,
  Card,
  DisplayText,
  Form,
  FormLayout,
  Frame,
  Layout,
  Page,
  PageActions,
  TextField,
  Toast,
} from '@shopify/polaris';
import store from 'store-js';
import gql from 'graphql-tag';
import { Mutation } from 'react-apollo';
import { Redirect } from '@shopify/app-bridge/actions';
import { Context, TitleBar } from '@shopify/app-bridge-react';

const UPDATE_PRICE = gql`
  mutation productVariantUpdate($input: ProductVariantInput!) {
    productVariantUpdate(input: $input) {
      product {
        title
      }
      productVariant {
        id
        price
      }
    }
  }
`;

class EditDiscountProduct extends React.Component {
  static contextType = Context;

  state = {
    discount: '',
    price: '',
    productId: '',
    variantId: '',
    showToast: false,
  };

  componentDidMount() {
    this.setState({ discount: this.itemToBeConsumed() });
  }

  render() {
    const app = this.context;
    const redirectToProduct = () => {
      const redirect = Redirect.create(app);
      redirect.dispatch(
        Redirect.Action.APP,
        '/list-discount-products',
      );
    };

    const { name, price, discount, variantId, productId } = this.state;

    return (
      <Mutation
        mutation={UPDATE_PRICE}
      >
        {(handleSubmit, {error, data}) => {
          const showError = error && (
            <Banner status="critical">{error.message}</Banner>
          );
          const showToast = data && data.productVariantUpdate && (
            <Toast
              content="Sucessfully updated"
              onDismiss={() => this.setState({ showToast: false })}
            />
          );

          return (
            <Frame>
              <Page>
                <TitleBar
                  title={`Edit ${name}`}
                  breadcrumbs={[{ content: "List Discount Products", url: "/list-discount-products" }]}
                  secondaryActions={[{
                    content: 'Back to Products',
                    url: '/list-discount-products'
                  }]}
                />
                <Layout>
                  {showToast}
                  <Layout.Section>
                    {showError}
                  </Layout.Section>
                  <Layout.Section>
                    <DisplayText size="large">Edit {name}</DisplayText>
                    <br/>
                    <Form>
                      <Card sectioned>
                        <FormLayout>
                          <FormLayout.Group>
                            <TextField
                              prefix="$"
                              value={price}
                              disabled={true}
                              label="Original price"
                              type="price"
                            />
                            <TextField
                              prefix="$"
                              value={discount}
                              onChange={this.handleChange('discount')}
                              label="Discounted price"
                              type="discount"
                            />
                          </FormLayout.Group>
                          <p>
                            This sale price will expire in two weeks
                          </p>
                        </FormLayout>
                      </Card>
                      <PageActions
                        primaryAction={[
                          {
                            content: 'Save',
                            onAction: () => {
                              const productVariableInput = {
                                id: variantId,
                                price: discount,
                              };
                              handleSubmit({
                                variables: { input: productVariableInput },
                              });
                              this.setState({ open: false });
                            }
                          }
                        ]}
                        secondaryActions={[
                          {
                            content: 'Remove discount',
                            onAction: () => {
                              const removeIds = store.get('discountIds').filter(id => !id.includes(productId));
                              store.set('discountIds', removeIds);
                              redirectToProduct();
                            }
                          }
                        ]}
                      />
                    </Form>
                  </Layout.Section>
                </Layout>
              </Page>
            </Frame>
          );
        }}
      </Mutation>
    );
  }

  handleChange = (field) => {
    return (value) => this.setState({ [field]: value });
  };

  itemToBeConsumed = () => {
    const item = store.get('editCustomProduct');
    const name = item.title;
    const price = item.variants.edges[0].node.price;
    const variantId = item.variants.edges[0].node.id;
    const productId = item.id;
    const discounter = price * 0.1;
    this.setState({ name, price, variantId, productId });
    return (price - discounter).toFixed(2);
  };
}

export default EditDiscountProduct;
