import React from 'react';
import {
  DisplayText,
  EmptyState,
  Layout,
  Page
} from '@shopify/polaris';
import store from 'store-js';
import ScriptCRUD from '../components/ScriptCRUD';

const img = 'https://cdn.shopify.com/s/files/1/0757/9955/files/empty-state.svg';

class EditScripts extends React.Component {
  render() {
    return (
      <Page>
        <ScriptCRUD/>
      </Page>
    );
  }
}

export default EditScripts;
