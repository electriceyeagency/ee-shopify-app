import React from 'react';
import {
  Button,
  Card,
  ColorPicker,
  Layout,
  Page,
  Select,
  Stack,
  TextStyle,
  Thumbnail,
} from '@shopify/polaris';
import store from 'store-js';
import { Context, ResourcePicker, TitleBar } from '@shopify/app-bridge-react';
import { EditMinor, PlusMinor } from '@shopify/polaris-icons';

class EditShortcode extends React.Component {
  static contextType = Context;

  state = {
    currentShortcode: false,
    shortcodes: false,
    open: false,
  }

  render() {
    return (
      <Page>
        <TitleBar
          title={`Edit Shortcode`}
          secondaryActions={[{
            content: 'Back to Shortcodes',
            url: '/list-shortcodes'
          }]}
        />
        {this.currentShortcode() ? (
          <Layout>
            <Layout.Section oneHalf>
              <Card sectioned title={`Edit ${this.state.currentShortcode.title} shortcode`}>
                <Layout>
                  {/* @TODO Loop through available sub colors (makes template more reusable), same with "styles" */}
                  <Layout.Section>
                    <h2>Title Color</h2>
                    <ColorPicker onChange={this.setTitleColor} color={this.state.currentShortcode.colors.title} />
                  </Layout.Section>
                  <Layout.Section>
                    <h2>Price Color</h2>
                    <ColorPicker onChange={this.setPriceColor} color={this.state.currentShortcode.colors.price} />
                  </Layout.Section>
                  <Layout.Section>
                    <h2>Font</h2>
                    <Select
                      title="Font"
                      options={[
                        { label: "Helvetica", value: "Helvetica" },
                        { label: "Times", value: "Times" },
                        { label: "Inherit", value: "inherit" },
                      ]}
                      value={this.state.currentShortcode.styles.font}
                      onChange={this.setFontStyle}
                    />
                  </Layout.Section>
                  <Layout.Section>
                    <h2>Product Select</h2>
                    <ResourcePicker
                      resourceType="Product"
                      selectMultiple={false}
                      showVariants={false}
                      open={this.state.open}
                      onSelection={(resources) => this.handleProductSelection(resources)}
                      onCancel={() => this.setState({ open: false })}
                    />
                  </Layout.Section>
                  {this.state.currentShortcode.product?.id ? (
                    <Layout.Section>
                      <Stack alignment="center">
                        <Thumbnail
                          source={
                            this.state.currentShortcode.product.data.images[0]
                              ? this.state.currentShortcode.product.data.images[0].originalSrc
                              : ''
                          }
                          alt={
                            this.state.currentShortcode.product.data.title
                              ? this.state.currentShortcode.product.data.title
                              : ''
                          }
                        />
                        <div>
                          <h2><strong>Product Selected:</strong></h2>
                          <h2>{this.state.currentShortcode.product.data.title}</h2>
                          <p>${this.state.currentShortcode.product.data.variants[0].price}</p>
                        </div>
                      </Stack>
                      <br />
                      <Button icon={EditMinor} onClick={() => this.setState({ open: true })}>Change Product</Button>
                    </Layout.Section>
                  ) : (
                    <Layout.Section>
                      <Button icon={PlusMinor} onClick={() => this.setState({ open: true })}>Select A Product</Button>
                    </Layout.Section>
                  )}
                </Layout>
              </Card>
            </Layout.Section>
            <Layout.Section oneHalf>
              <Card sectioned title={`${this.state.currentShortcode.title} Shortcode Demo`}>
                <Layout>
                  <Layout.Section>
                    {this.state.currentShortcode.product?.id ? (
                      <Stack>
                        <img
                          style={{ width: '100%', paddingRight: '1.6rem' }}
                          src={
                            this.state.currentShortcode.product.data.images[0]
                              ? this.state.currentShortcode.product.data.images[0].originalSrc
                              : ''
                          }
                          alt={
                            this.state.currentShortcode.product.data.title
                              ? this.state.currentShortcode.product.data.title
                              : ''
                          }
                        />
                        <div style={{ fontFamily: this.state.currentShortcode.styles.font }}>
                          <h2 style={{ color: this.generateHsl(this.state.currentShortcode.colors.title) }}><strong>{this.state.currentShortcode.product.data.title}</strong></h2>
                          <p style={{ color: this.generateHsl(this.state.currentShortcode.colors.price) }}><strong>${this.state.currentShortcode.product.data.variants[0].price}</strong></p>
                          <br />
                          <div dangerouslySetInnerHTML={{__html: this.state.currentShortcode.product.data.descriptionHtml}}></div>
                          <br />
                          <button style={{ fontFamily: this.state.currentShortcode.styles.font }}>Add To Cart</button>
                        </div>
                      </Stack>
                    ): (<i>Demo will show once product is selected</i>)}
                  </Layout.Section>
                </Layout>
              </Card>
              <Card sectioned title={`How to Use ${this.state.currentShortcode.title} Shortcode`}>
                <Layout>
                  <Layout.Section>
                    <h2>Simply copy/paste this code into your blog post</h2>
                    <p>NOTE: ensure you add this in the code view, not editor view</p>
                    <br/>
                    <p>{this.generateShortcodeMarkup()}</p>
                  </Layout.Section>
                </Layout>
              </Card>
            </Layout.Section>
          </Layout>
        ) : (
          <Layout>
            <Layout.Section>
              <Card>
                <p>Loading...</p>
              </Card>
            </Layout.Section>
          </Layout>
        )}
      </Page>
    );
  }

  setTitleColor = (e) => {
    this.state.currentShortcode.colors.title = e
    store.set('currentShortcode', this.state.currentShortcode)
    this.setState({ currentShortcode: this.state.currentShortcode })
    this.updateShortcodes()
  }

  setPriceColor = (e) => {
    this.state.currentShortcode.colors.price = e
    store.set('currentShortcode', this.state.currentShortcode)
    this.setState({ currentShortcode: this.state.currentShortcode })
    this.updateShortcodes()
  }

  setFontStyle = (e) => {
    this.state.currentShortcode.styles.font = e
    store.set('currentShortcode', this.state.currentShortcode)
    this.setState({ currentShortcode: this.state.currentShortcode })
    this.updateShortcodes()
  }

  handleProductSelection = (resources) => {
    this.state.currentShortcode.product.data = resources.selection[0]
    this.state.currentShortcode.product.id = resources.selection[0].id
    store.set('currentShortcode', this.state.currentShortcode)
    this.setState({ open: false, currentShortcode: this.state.currentShortcode })
    this.updateShortcodes()
  };

  currentShortcode = () => {
    const shortcode = store.get('currentShortcode');
    if(!shortcode || shortcode.length <= 0) return false
    this.state.currentShortcode = shortcode

    return shortcode
  };

  hsv_to_hsl(h, s, v) {
    // both hsv and hsl values are in [0, 1]
    var l = (2 - s) * v / 2;

    if (l != 0) {
        if (l == 1) {
            s = 0;
        } else if (l < 0.5) {
            s = s * v / (l * 2);
        } else {
            s = s * v / (2 - l * 2);
        }
    }

    return `hsl(${h}, ${s * 100}%, ${l * 100}%)`;
  }

  generateHsl = (val) => {
    return this.hsv_to_hsl(val.hue, val.saturation, val.brightness)
  }

  updateShortcodes = () => {
    const shortcode = store.get('currentShortcode');
    let shortcodes = store.get('shortcodes')
    if(shortcodes && shortcode) {
      shortcodes.forEach((code, index) => {
        if(code.id == shortcode.id) {
          shortcodes[index] = shortcode
        }
      })

      store.set('shortcodes', shortcodes)
      this.setState({ shortcodes: shortcodes })
      console.log(store.get('shortcodes'))
    }
  }

  generateShortcodeMarkup = () => {
    const newShortcode = this.state.currentShortcode.code
    .replace('product_title', this.generateHsl(this.state.currentShortcode.colors.title))
    .replace('product_price', this.generateHsl(this.state.currentShortcode.colors.price))
    .replace('product_font', this.state.currentShortcode.styles.font)

    if(this.state.currentShortcode?.product?.data && Object.keys(this.state.currentShortcode.product.data).length > 0) {
      newShortcode.replace('product_handle', this.state.currentShortcode.product.data.handle)
    } else {
      newShortcode.replace('product_handle', '')
    }

    return newShortcode
  }
}

export default EditShortcode;
