import React from 'react';
import { Context, TitleBar } from '@shopify/app-bridge-react';
import { Redirect } from '@shopify/app-bridge/actions';
import { Layout, Card, EmptyState, Page, TextStyle } from '@shopify/polaris';

class Index extends React.Component {
  static contextType = Context;

  render() {
    const app = this.context;
    const redirectToListProducts = () => {
      const redirect = Redirect.create(app);
      redirect.dispatch(
        Redirect.Action.APP,
        '/list-discount-products',
      );
    };
    const redirectToListShortcodes = () => {
      const redirect = Redirect.create(app);
      redirect.dispatch(
        Redirect.Action.APP,
        '/list-shortcodes',
      );
    };
    const redirectToCustomizeProducts = () => {
      const redirect = Redirect.create(app);
      redirect.dispatch(
        Redirect.Action.APP,
        '/list-custom-products',
      );
    };
    const redirectToEditScripts = () => {
      const redirect = Redirect.create(app);
      redirect.dispatch(
        Redirect.Action.APP,
        '/edit-scripts',
      );
    };

    return (
      <Page>
        <TitleBar
          title="Electric Eye Application"
        />
        <Layout>
          <Layout.Section primary>
            <Card>
              <EmptyState
                heading="Electric Eye Application"
                image="https://cdn.shopify.com/s/files/1/0016/1090/7683/files/ee-emptystate.png"
              >
                <p>Electric Eye uses this private, custom Shopify app to provide advanced functionality to your site.</p>
              </EmptyState>
            </Card>
          </Layout.Section>
  
          <Layout.Section>
            <Layout>
              <Layout.Section secondary>
                <Card
                  title="Customize Shortcodes"
                  primaryFooterAction={{
                    content: 'Customize Shortcodes',
                    onAction: () => redirectToListShortcodes()
                  }}
                >
                  <Card.Section>
                    <TextStyle>
                      View, and Customize Shortcodes
                    </TextStyle>
                  </Card.Section>
                </Card>
              </Layout.Section>
              {/* <Layout.Section secondary>
                <Card
                  title="Discount Products"
                  primaryFooterAction={{
                    content: 'Discount',
                    onAction: () => redirectToListProducts()
                  }}
                >
                  <Card.Section>
                    <TextStyle>
                      Add a discount to your products; expires in 2 weeks.
                    </TextStyle>
                  </Card.Section>
                </Card>
              </Layout.Section> */}
              <Layout.Section secondary>
                <Card
                  title="Customize Product Pages"
                  primaryFooterAction={{
                    content: 'Customize',
                    onAction: () => redirectToCustomizeProducts()
                  }}
                >
                  <Card.Section>
                    <TextStyle>
                      Enhance your products detail pages with custom functionality.
                    </TextStyle>
                  </Card.Section>
                </Card>
              </Layout.Section>
              <Layout.Section secondary>
                <Card
                  title="EE Scripts"
                  primaryFooterAction={{
                    content: 'Manage',
                    onAction: () => redirectToEditScripts()
                  }}
                >
                  <Card.Section>
                    <TextStyle>
                      Add, or Update EE app's frontend code
                    </TextStyle>
                  </Card.Section>
                </Card>
              </Layout.Section>
            </Layout> 
          </Layout.Section>
        </Layout>
      </Page>
    );
  };
}

export default Index;
