import React from 'react';
import {
  Button,
  Card,
  Layout,
  Page,
  ResourceList,
  Stack,
  TextStyle,
} from '@shopify/polaris';
import store from 'store-js';
import { EditMinor } from '@shopify/polaris-icons';
import { Redirect } from '@shopify/app-bridge/actions';
import { Context, TitleBar } from '@shopify/app-bridge-react';

class ListShortcodes extends React.Component {
  static contextType = Context;

  state = {
    shortcodes: [
      {
        id: 1,
        title: "Featured Product",
        code: '[product handle="product_handle" title="product_title" price="product_price" font="product_font"]',
        product: {
          id: '',
          data: {}
        },
        colors: {
          title: {
            hue: 120,
            brightness: 1,
            saturation: 1,
          },
          price: {
            hue: 120,
            brightness: 1,
            saturation: 1,
          },
        },
        styles: {
          font: 'Helvetica',
          image_border_radius: 0
        }
      }
    ]
  };

  componentDidMount() {
    // uncomment to reset data
    // store.set('shortcodes', this.state.shortcodes);

    const shortcodes = store.get('shortcodes')
    if(shortcodes) {
      this.setState({ shortcodes: shortcodes })
    } else {
      store.set('shortcodes', this.state.shortcodes);
    }
  }

  render() {
    return (
      <Page>
        <TitleBar
          title={`Edit Shortcodes`}
          secondaryActions={[{
            content: 'Back to Menu',
            url: '/'
          }]}
        />
        <Layout>
          <Layout.Section>
            <Card>
              {!this.isStateEmpty() ? (
                // @TODO https://polaris.shopify.com/components/index-table
                <ResourceList
                  showHeader
                  resourceName={{ singular: 'Shortcode', plural: 'Shortcodes' }}
                  items={this.state.shortcodes}
                  renderItem={item => {
                    return (
                      <ResourceList.Item
                        id={item.id}
                        accessibilityLabel={`${item.title}`}
                      >
                        <Stack alignment="center" distribution="fill">
                          <Stack.Item>
                            <h3>
                              <TextStyle variation="strong">
                                {item.title}
                              </TextStyle>
                            </h3>
                          </Stack.Item>
                          {/* <Stack.Item>
                            <span>{item.code}</span>
                          </Stack.Item> */}
                          <Stack.Item>
                            <p>{Object.keys(item.colors).length} Colors</p>
                          </Stack.Item>
                          <Stack.Item>
                            <p>{Object.keys(item.styles).length} Styles</p>
                          </Stack.Item>
                          <Stack.Item>
                            <p>{item.styles.font} Font</p>
                          </Stack.Item>
                          <Stack.Item>
                            <Button icon={EditMinor} onClick={() => { this.goToEditShortcode(item) }}></Button>
                          </Stack.Item>
                        </Stack>
                      </ResourceList.Item>
                    )
                  }}
                  />
                ) : (
                  <p>Loading...</p>
                )}
            </Card>
          </Layout.Section>
        </Layout>
      </Page>
    );
  }

  isStateEmpty = () => {
    const shortcodes = store.get('shortcodes');
    if(!shortcodes || shortcodes.length <= 0) return true
  };

  goToEditShortcode = (item) => {
    store.set('currentShortcode', item);
    store.set('shortcodes', this.state.shortcodes);

    const app = this.context;
    const redirect = Redirect.create(app);
    redirect.dispatch(
      Redirect.Action.APP,
      '/edit-shortcode',
    );
  };
}

export default ListShortcodes;
