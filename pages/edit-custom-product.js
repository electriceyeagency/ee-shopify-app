import React from 'react';
import {
  Button,
  Banner,
  Card,
  DisplayText,
  Form,
  FormLayout,
  Frame,
  Layout,
  Page,
  PageActions,
  TextField,
  Toast,
} from '@shopify/polaris';
import store from 'store-js';
import gql from 'graphql-tag';
import { Mutation } from 'react-apollo';
import { Redirect } from '@shopify/app-bridge/actions';
import { Context, TitleBar, ResourcePicker } from '@shopify/app-bridge-react';
import UpsellProductsList from '../components/UpsellProductsList';
import BundleProductsList from '../components/BundleProductsList';

const UPDATE_METAFIELDS = gql`
  mutation($input: ProductInput!) {
    productUpdate(input: $input) {
      product {
        metafields(first: 100) {
          edges {
            node {
              id
              namespace
              key
              value
            }
          }
        }
      }
    }
  }
`;

class EditCustomProduct extends React.Component {
  static contextType = Context;

  state = {
    openUpsell: false,
    openBundle: false,
    discount: '',
    price: '',
    productId: '',
    showToast: false,
    showDeleteToast: false
  };

  componentDidMount() {
    this.itemToBeConsumed()
  }

  render() {
    const app = this.context;
    const redirectToProduct = () => {
      const redirect = Redirect.create(app);
      redirect.dispatch(
        Redirect.Action.APP,
        '/list-custom-products',
      );
    };

    const { 
      openBundle, 
      openUpsell,
      name, 
      price, 
      discount, 
      productId, 
      showToast,
      showDeleteToast
    } = this.state;

    return (
      <Mutation
        mutation={UPDATE_METAFIELDS}
      >
        {(handleSubmit, {error, data}) => {
          const showError = error && (
            <Banner status="critical">{error.message}</Banner>
          );

          const updatedToast = data && data.productUpdate && showToast && (
            <Toast
              content="Sucessfully updated"
              onDismiss={() => this.setState({ showToast: false })}
            />
          );

          const deleteToast = showDeleteToast && (
            <Toast
              content="Sucessfully deleted"
              onDismiss={() => this.setState({ showDeleteToast: false })}
            />
          );

          return (
            <Frame>
              <Page>
                <TitleBar
                  title={`Edit ${name}`}
                  breadcrumbs={[{ content: "List Custom Products", url: "/list-custom-products" }]}
                  secondaryActions={[{
                    content: 'Back to Products',
                    url: '/list-custom-products'
                  }]}
                />
                <Layout>
                  {updatedToast}
                  {deleteToast}
                  <Layout.Section>
                    {showError}
                  </Layout.Section>
                  <Layout.Section>
                    <DisplayText size="large">Edit {name}</DisplayText>
                    <br/>
                    <Form>
                      <Card sectioned>
                        <FormLayout>
                          <DisplayText size="large">Upsell Products</DisplayText>
                          <p>
                            Select products to upsell on {name}. Listed as <strong>Frequently Bought Together</strong> on the frontend.
                          </p>
                          {openUpsell ?
                            <ResourcePicker
                              key="upsell"
                              resourceType="Product"
                              showVariants={true}
                              open={true}
                              onSelection={(resources) => this.handleUpsellSelection(resources, handleSubmit)}
                              onCancel={() => this.setState({ openUpsell: false })}
                            />
                          : ''}
                          {this.upsellProducts().length > 0 ? 
                            <UpsellProductsList 
                              deleteToast={this.deleteToast.bind(this)} 
                              rerender={this.rerender.bind(this)}
                            /> 
                          : ''}
                          <Button onClick={() => this.setState({ openUpsell: true })}>Add product(s)</Button>
                        </FormLayout>
                      </Card>

                      <Card sectioned>
                        <FormLayout>
                          <DisplayText size="large">Bundle Products</DisplayText>
                          <p>
                            Select products to bundle together. Listed as <strong>Bundle Together</strong> on the frontend, and <strong>added to cart together</strong>, with the ability to add/remove specific options.
                          </p>
                          {openBundle ?
                            <ResourcePicker
                              id="bundle"
                              resourceType="Product"
                              showVariants={true}
                              open={true}
                              onSelection={(resources) => this.handleBundleSelection(resources, handleSubmit)}
                              onCancel={() => this.setState({ openBundle: false })}
                            />  
                          : ''}
                          {this.bundleProducts().length > 0 ? 
                            <BundleProductsList 
                              deleteToast={this.deleteToast.bind(this)} 
                              rerender={this.rerender.bind(this)}
                            /> 
                          : ''}
                          <Button onClick={() => this.setState({ openBundle: true })}>Add product(s)</Button>
                        </FormLayout>
                      </Card>

                      <Card sectioned>
                        <FormLayout>
                          <DisplayText size="large">Additional Metafields</DisplayText>
                          <p></p>
                          <p>METAFIELD FORM FIELDS GO HERE</p>
                          <p>Product details section: image & description</p>
                        </FormLayout>
                      </Card>
                      <PageActions
                        secondaryActions={[
                          {
                            content: 'Remove Customizations',
                            disabled: this.isRemoveDisabled(),
                            onAction: () => {
                              const removeIds = store.get('customIds').filter(id => !id.includes(productId));
                              store.set('customIds', removeIds);
                              redirectToProduct();
                            }
                          }
                        ]}
                      />
                    </Form>
                  </Layout.Section>
                </Layout>
              </Page>
            </Frame>
          );
        }}
      </Mutation>
    );
  }

  upsellProducts = () => {
    return store.get('upsellIds') || []
  };

  bundleProducts = () => {
    return store.get('bundleIds') || []
  };

  isRemoveDisabled = () => {
    let removeDisabled = false;
    if(
      !store.get('upsellIds') 
      || store.get('upsellIds').length > 0
      || !store.get('bundleIds') 
      || store.get('bundleIds').length > 0
    ) removeDisabled = true;

    return removeDisabled
  };

  handleUpsellSelection = (resources, handleSubmit) => {
    const idsFromResources = resources.length > 0 && resources.selection.map((product) => product.id) || [];
    const existingIds = store.get('upsellIds');
    const existingData = store.get('upsellData');
    store.set('upsellIds', [...existingIds, ...idsFromResources]);
    store.set('upsellData', [...existingData, ...resources.selection]);

    this.saveMetafields(handleSubmit, 'upsell')
  };

  handleBundleSelection = (resources, handleSubmit) => {
    const idsFromResources = resources.length > 0 && resources.selection.map((product) => product.id) || [];
    const existingIds = store.get('bundleIds');
    const existingData = store.get('bundleData');
    store.set('bundleIds', [...existingIds, ...idsFromResources]);
    store.set('bundleData', [...existingData, ...resources.selection]);

    this.saveMetafields(handleSubmit, 'bundle')
  };

  saveMetafields = (handleSubmit, namespace) => {
    const localMetafields = [];
    const products = namespace === 'upsell' ? store.get('upsellData') : store.get('bundleData');

    products.forEach((product, index) => {
      let newMetafield = {
        namespace: namespace,
        key: product.handle,
        value: JSON.stringify(product),
        valueType: 'STRING'
      }

      const existingMetafield = store.get('metafields').find(field => field.key === product.handle)
      if(existingMetafield) {
        newMetafield.id = existingMetafield.id
      }

      localMetafields.push(newMetafield);
    });

    handleSubmit({
      variables: { input: { id: this.state.productId, metafields: localMetafields }},
    }).then(res => {
      this.setState({ openBundle: false });
      this.setState({ openUpsell: false });
      this.setState({ showToast: true });

      const upsellIds = [];
      const upsellData = [];
      const bundleIds = [];
      const bundleData = [];
      const metafields = [];
      res.data.productUpdate.product.metafields.edges.forEach(field => {
        if(field.node.namespace === 'upsell') {
          var productJSON = JSON.parse(field.node.value);
          upsellIds.push(productJSON.id);
          upsellData.push(productJSON);
          metafields.push(field.node);
        } else if(field.node.namespace === 'bundle') {
          var productJSON = JSON.parse(field.node.value);
          bundleIds.push(productJSON.id);
          bundleData.push(productJSON);
          metafields.push(field.node);
        }
      })

      store.set('upsellIds', upsellIds);
      store.set('upsellData', upsellData);
      store.set('bundleIds', bundleIds);
      store.set('bundleData', bundleData);
      store.set('metafields', metafields);

      this.forceUpdate();
    })
  };

  handleChange = (field) => {
    return (value) => this.setState({ [field]: value });
  };

  itemToBeConsumed = () => {
    const item = store.get('editCustomProduct');
    const name = item.title;
    const productId = item.id;
    this.setState({ item, name, productId });

    if(item.metafields.edges.length > 0) {
      const upsellIds = [];
      const upsellData = [];
      const bundleIds = [];
      const bundleData = [];
      const metafields = [];
      item.metafields.edges.forEach(field => {
        if(field.node.namespace === 'upsell') {
          var productJSON = JSON.parse(field.node.value);
          upsellIds.push(productJSON.id);
          upsellData.push(productJSON);
          metafields.push(field.node)
        } else if(field.node.namespace === 'bundle') {
          var productJSON = JSON.parse(field.node.value);
          bundleIds.push(productJSON.id);
          bundleData.push(productJSON);
          metafields.push(field.node);
        }
      })

      store.set('upsellIds', upsellIds);
      store.set('upsellData', upsellData);
      store.set('bundleIds', bundleIds);
      store.set('bundleData', bundleData);
      store.set('metafields', metafields);

      this.forceUpdate();
    } else {
      store.set('upsellIds', []);
      store.set('upsellData', []);
      store.set('bundleIds', []);
      store.set('bundleData', []);
      store.set('metafields', []);
    }
  };

  deleteToast = () => {
    this.setState({ showDeleteToast: true });
  }

  rerender = () => {
    this.forceUpdate();
  }
}

export default EditCustomProduct;
