import React from 'react';
import { EmptyState, Layout, Page } from '@shopify/polaris';
import { ResourcePicker, TitleBar, Context } from '@shopify/app-bridge-react';
import { Redirect } from '@shopify/app-bridge/actions';
import store from 'store-js';
import DiscountProductsList from '../components/DiscountProductsList';

const img = 'https://cdn.shopify.com/s/files/1/0757/9955/files/empty-state.svg';

class ListDiscountProducts extends React.Component {
  static contextType = Context;
  state = { open: false };

  render() {
    // NOTE currently not used
    const app = this.context;
    const redirectToHome = () => {
      const redirect = Redirect.create(app);
      redirect.dispatch(
        Redirect.Action.APP,
        '/',
      );
    };

    return (
      <Page>
        <TitleBar
          title="List Custom Products"
          primaryAction={{
            content: 'Select products',
            onAction: () => this.setState({ open: true }),
          }}
          secondaryActions={[{
            content: 'Back to Menu',
            url: '/'
          }]}
        />
        <ResourcePicker
          resourceType="Product"
          showVariants={false}
          open={this.state.open}
          onSelection={(resources) => this.handleSelection(resources)}
          onCancel={() => this.setState({ open: false })}
        />
        {this.isStateEmpty() ? (
          <Layout>
            <EmptyState
              heading="Discount your products temporarily"
              action={{
                content: 'Select products',
                onAction: () => this.setState({ open: true }),
              }}
              image={img}
            >
              <p>Select products to change their price temporarily.</p>
            </EmptyState>
          </Layout>
        ) : (
          <DiscountProductsList />
        )}
      </Page>
    );
  };

  handleSelection = (resources) => {
    const idsFromResources = resources.selection.map((product) => product.id);
    this.setState({ open: false });
    store.set('discountIds', idsFromResources);
  };

  isStateEmpty = () => {
    const discountIds = store.get('discountIds');
    if(!discountIds || discountIds.length <= 0) return true
  };
}

export default ListDiscountProducts;
