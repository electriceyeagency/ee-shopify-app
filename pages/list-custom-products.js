import React from 'react';
import { EmptyState, Layout, Page } from '@shopify/polaris';
import { ResourcePicker, TitleBar, Context } from '@shopify/app-bridge-react';
import { Redirect } from '@shopify/app-bridge/actions';
import CustomProductsList from '../components/CustomProductsList';
import store from 'store-js';

const img = 'https://cdn.shopify.com/s/files/1/0757/9955/files/empty-state.svg';

class ListCustomProducts extends React.Component {
  static contextType = Context;
  state = { open: false };

  render() {
    // NOTE currently not used
    const app = this.context;
    const redirectToHome = () => {
      const redirect = Redirect.create(app);
      redirect.dispatch(
        Redirect.Action.APP,
        '/',
      );
    };

    return (
      <Page>
        <TitleBar
          title="List Custom Products"
          primaryAction={{
            content: 'Add products',
            onAction: () => this.setState({ open: true }),
          }}
          secondaryActions={[{
            content: 'Back to Menu',
            url: '/'
          }]}
        />
        <ResourcePicker
          resourceType="Product"
          showVariants={false}
          open={this.state.open}
          onSelection={(resources) => this.handleSelection(resources)}
          onCancel={() => this.setState({ open: false })}
        />
        {this.isStateEmpty() ? (
          <Layout>
            <EmptyState
              heading="Customize Products"
              action={{
                content: 'Add products',
                onAction: () => this.setState({ open: true }),
              }}
              image={img}
            >
              <p>Select products to customize them.</p>
            </EmptyState>
          </Layout>
        ) : (
          <CustomProductsList />
        )}
      </Page>
    );
  };

  handleSelection = (resources) => {
    const idsFromResources = resources.selection.map((product) => product.id) || [];
    const existingIds = store.get('customIds') || [];
    store.set('customIds', [ ...existingIds, ...idsFromResources ]);
    this.setState({ open: false });
  };

  isStateEmpty = () => {
    const customIds = store.get('customIds');
    if(!customIds || customIds.length <= 0) return true
  };
}

export default ListCustomProducts;
