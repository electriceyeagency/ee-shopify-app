require("dotenv").config();
const webpack = require('webpack');
const { default: Shopify } = require('@shopify/shopify-api');

// https://community.shopify.com/c/Shopify-APIs-SDKs/Shopify-App-React-and-NodeJS-tutorial-API-KEY-not-defined/td-p/770584
const apiKey = JSON.stringify(process.env.SHOPIFY_API_KEY);

module.exports = {
  webpack: (config) => {
    const env = { API_KEY: apiKey };
    config.plugins.push(new webpack.DefinePlugin(env));
    return config;
  },
};
